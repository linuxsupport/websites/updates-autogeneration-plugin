from setuptools import setup, find_packages


setup(
    name='updates-autogeneration-plugin',
    version='0.1.0',
    description='Auto generation of latest updates for distros',
    long_description='',
    keywords='mkdocs',
    url='',
    author='Daniel Juarez Gonzalez',
    author_email='djuarezg@cern.ch',
    license='MIT',
    python_requires='>=2.7',
    install_requires=[
        'mkdocs>=1.1'
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    packages=find_packages(),
    entry_points={
        'mkdocs.plugins': [
            'updates-autogeneration-plugin = updates_autogeneration_plugin.plugin:UpdatesAutogenerationPlugin'
        ]
    }
)
